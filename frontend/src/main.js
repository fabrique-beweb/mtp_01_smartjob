import Vue from "vue";
import App from "./App.vue";
import router from "./router/router.js";
import Axios from "axios";
import store from "./store.js";
import bootstrapvue from "bootstrap-vue";
import VModal from "vue-js-modal";
import vuetify from "./plugins/vuetify";
import "vuetify/dist/vuetify.min.css";

Vue.prototype.$http = Axios;
const token = localStorage.getItem("token");
if (token) {
  Vue.prototype.$http.defaults.headers.common["Authorization"] = token;
}

Vue.config.productionTip = false;

//utilisation des modules
Vue.use(bootstrapvue);
Vue.use(VModal);
Vue.use(vuetify);

//importation bus pour évènements
Vue.config.productionTip = false
export const bus = new Vue();


new Vue({
  render: h => h(App),
  vuetify,
  router,
  store

}).$mount("#app");
