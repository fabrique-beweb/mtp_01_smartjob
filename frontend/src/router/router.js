import Vue from "vue";
import Router from "vue-router";
import store from "../store.js";
import Home from "../components/home/Home.vue";
import RegistrationStepOne from "../components/forms/RegistrationStepOne.vue";
import RegistrationStepTwo from "../components/forms/RegistrationStepTwo";
import RegistrationStepThree from "../components/forms/RegistrationStepThree";
import Login from "../components/forms/Login.vue";
import ProfilUser from "../components/profilUser/ProfilUser.vue";
import JobOffer from "../components/jobOffers/jobOffer.vue";

Vue.use(Router);

//si l'utilisateur est auhentifié, fonction du store
const ifNotAuthenticated = (to, from, next) => {
  if (!store.getters.isAuthenticated) {
    next();
    return;
  }
  next("/");
};

//si l'utilisateur n'est pas auhentifié, fonction du store
const ifAuthenticated = (to, from, next) => {
  if (store.getters.isAuthenticated) {
    next();
    return;
  }
  next('/')
}

export default new Router({
  
  //définit les routes et les accès
  routes: [
    { path: "/", name: "home", component: Home },
    {
      path: "/registration_step_1",
      name: "register_one",
      component: RegistrationStepOne,
      beforeEnter: ifNotAuthenticated
    },
    {
      path: "/registration_step_2",
      name: "register_two",
      component: RegistrationStepTwo,
      beforeEnter: ifAuthenticated
    },
    {
      path: "/registration_step_3",
      name: "register_three",
      component: RegistrationStepThree,
      beforeEnter: ifAuthenticated
    },
    {
      path: "/login",
      name: "login",
      component: Login,
      beforeEnter: ifNotAuthenticated
    },
    {
      path: "/api/user/profil",
      name: "profil_user",
      component: ProfilUser,
      beforeEnter: ifAuthenticated
    },
    {
      path: "/job_offer",
      name: "job_offer",
      component: JobOffer,
      beforeEnter: ifAuthenticated
    },
  ],
  mode: "history" // pour enlever le '#' dans l'url
});