//pour info
[
    {
        "alternance": false,
        "appellationlibelle": "Ingénieur / Ingénieure d'étude informatique",
        "competences": [],
        "dateActualisation": "2019-11-21T03:52:52+01:00",
        "dateCreation": "2019-11-21T03:52:52+01:00",
        "description": "Description du poste :\nLynx RH Montpellier, cabinet de recrutement en CDI, CDD, Intérim, recherche pour son client, un groupe indépendant d'ingénierie spécialisé dans les projets d'aménagement urbain (MOE), un Ingénieur / Chargé(e) études VRD, basé à Montpellier (34).\nVous travaillerez sur des projets de VRD d'accompagnement :\n- Réaliser les plans, études et mémoires de réseaux d'eau potable, d'assainissement & de VRD pour les phases d'Avant-Projet & Projet avec investigations terrain\n- Préparer les pièces écrites pour la consultation des entreprises (devis, plans, CCTP...)\n- Analyser les offres des entreprises\n- Préparer les réponses aux consultations de Maîtrise d'œuvre\n- Effectuer une veille et mise à jour des pièces techniques et administratives\nDescription du profil :\nVous êtes issu d'une formation de type Bac, DUT, BTS, Master, Ecole d'ingénieur.\nConnaissance de l'outil informatique (Word, Excel, Autocad, Covadis, Epanet, Infographie...).\nPermis de conduite exigé",
        "dureeTravailLibelleConverti": "Non renseigné",
        "entreprise": {
            "nom": "Lynx Rh"
        },
        "experienceExige": "D",
        "experienceLibelle": "Débutant accepté",
        "formations": [],
        "id": "3947919",
        "intitule": "Ingénieur (H/F)",
        "langues": [],
        "lieuTravail": {
            "codePostal": "34000",
            "commune": "34172",
            "latitude": 43.59861111,
            "libelle": "34 - MONTPELLIER",
            "longitude": 3.896944444
        },
        "natureContrat": "Contrat travail",
        "nombrePostes": 1,
        "origineOffre": {
            "origine": "2",
            "partenaires": [
            {
                "logo": "https://www.pole-emploi.fr/static/img/partenaires/meteojob80.png",
                "nom": "METEOJOB",
                "url": "https://www.meteojob.com/candidat/offres/offre-d-emploi-charge-d-etudes-h-f-montpellier-languedoc-roussillon-interim-11525333"
            }
            ],
            "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/3947919"
        },
        "qualificationCode": "X",
        "romeCode": "M1805",
        "romeLibelle": "Études et développement informatique",
        "secteurActivite": "78",
        "secteurActiviteLibelle": "Activités des agences de travail temporaire",
        "typeContrat": "MIS",
        "typeContratLibelle": "Mission intérimaire - 6 Mois"
    },
    {
        "accessibleTH": false,
        "alternance": false,
        "appellationlibelle": "Analyste en cybersécurité",
        "competences": [
            {
            "code": "121398",
            "exigence": "S",
            "libelle": "Analyser des problèmes techniques"
            },
            {
            "code": "121055",
            "exigence": "S",
            "libelle": "Déterminer des mesures correctives"
            },
            {
            "code": "120370",
            "exigence": "S",
            "libelle": "Déterminer les phases et procédures de tests techniques et fonctionnels de programmes et applications informatiques"
            },
            {
            "code": "117309",
            "exigence": "S",
            "libelle": "Établir un cahier des charges"
            },
            {
            "code": "124517",
            "exigence": "S",
            "libelle": "Rédiger une Spécification Technique de Besoin (STB)"
            }
        ],
        "contact": {
            "coordonnees1": "http://www.jobs.net/j/JrkJvtZu?idpartenaire=10",
            "urlPostulation": "http://www.jobs.net/j/JrkJvtZu?idpartenaire=10"
        },
        "dateCreation": "2019-11-20T16:45:58+01:00",
        "description": "Vous participez à la mise en place et au contrôle des process du Product Security Operating Center (PSOC), dédiés à la bonne gestion de la Cybersécurité et de la protection des données dans nos produits.\nVous identifiez et mettez en œuvre des outils pour les analyses de vulnérabilités et de sécurité, et effectuez l'évaluation de nos produits.\nVous participez à la définition et la maintenance des exigences de Cybersécurité pour nos produits, et proposez les architectures et solutions techniques adaptées.\nDoté(e) d'une formation supérieure, vous justifiez d'une première expérience dans la sécurité informatique pour des applications industrielles.\nVous maîtrisez les fondamentaux de la sécurité des systèmes et réseaux, de la PKI, de l'évaluation des vulnérabilités, des tests d'intrusion, du chiffrement, de l'authentification, des protections anti-malwares \nVous avez des connaissances en sécurité des applications, système d'exploitation (Windows, Linux), bases de données, etc.\n",
        "dureeTravailLibelle": "35H Horaires normaux",
        "dureeTravailLibelleConverti": "Temps plein",
        "entreprise": {
            "nom": "HORIBA MEDICAL"
        },
        "experienceExige": "D",
        "experienceLibelle": "Débutant accepté",
        "formations": [],
        "id": "096DYLD",
        "intitule": "Analyste en cybersécurité",
        "langues": [
            {
            "exigence": "S",
            "libelle": "Anglais Correct Souhaité"
            }
        ],
        "lieuTravail": {
            "codePostal": "34000",
            "commune": "34172",
            "latitude": 43.59861111,
            "libelle": "34 - MONTPELLIER",
            "longitude": 3.896944444
        },
        "natureContrat": "Contrat travail",
        "nombrePostes": 1,
        "origineOffre": {
            "origine": "1",
            "partenaires": [],
            "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/096DYLD"
        },
        "qualificationCode": "9",
        "qualificationLibelle": "Cadre",
        "qualitesProfessionnelles": [
            {
            "description": "Capacité à transmettre clairement des informations, à échanger, à écouter activement, à réceptionner des informations et messages et à faire preuve d’ouverture d’esprit. Exemple : être à l’écoute, être attentif aux autres",
            "libelle": "Sens de la communication"
            },
            {
            "description": "Capacité à prendre en charge son activité sans devoir être encadré de façon continue. Exemple : travailler efficacement sans responsable",
            "libelle": "Autonomie"
            },
            {
            "description": "Capacité à être proactif, à initier, à imaginer des propositions nouvelles pour résoudre les problèmes identifiés ou pour améliorer une situation. Exemple : proposer des améliorations, être positif et constructif",
            "libelle": "Force de proposition"
            }
        ]
    }
]