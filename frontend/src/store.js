import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

//Vuex similaire à Redux
Vue.use(Vuex);

export default new Vuex.Store({
    state: {
      status: '',
      token: localStorage.getItem('token'),
      dataCv: localStorage.getItem('res')
    },

    mutations: {
        //check login
        auth_request(state){
            state.status = 'loading'
          },
          auth_success(state, token, user){
            state.status = 'success'
            state.token = token
            state.user = user
          },
          auth_error(state){
            state.status = 'error'
          },
          logout(state){
            state.status = ''
            state.token = ''
          },
    },

    actions: {

        validate({commit}, data){

            return new Promise((resolve, reject) => {
              commit('auth_request')
              //log l'utilisateur et renvoi un token
              axios({url: 'http://localhost:8000/api/login_check', data: data, method: 'POST' })
              .then(resp => {
                const token = resp.data.token
                //stocke le token dans le local storage
                localStorage.setItem('token', token)
                axios.defaults.headers.common['Authorization'] = token
                commit('auth_success', token)
                resolve(resp)
              })
              .catch(err => {
                commit('auth_error')
                //supprime le token du local storage
                localStorage.removeItem('token')
                reject(err)
              })
            })
        },
        //deconnexion
        logout({commit}){
            return new Promise((resolve, reject) => {
              commit('logout')
              //vide le local storage
              localStorage.removeItem('token')
              localStorage.removeItem('res')
              localStorage.removeItem('TabCodeslocations')
              localStorage.removeItem('TabCodesMetiers')
              delete axios.defaults.headers.common['Authorization']
              resolve()
              .catch(err => {
                  commit('error')
                  localStorage.removeItem('token')
                  reject(err)
              })
            })
          }
    },
    //fonctions check statuts
    getters : {
        isAuthenticated: state => !!state.token,
        authStatus: state => state.status,
    }
  })