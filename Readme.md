Le projet SmartJob a été réalisé dans le cadre du projet externe. 

C’était un projet d’une durée de 4 semaines à réaliser pour un client. 
Ce dernier nous a demandé de réaliser le cahier des charges. Il comporte : 
-	La réalisation d’un site fullstack permettant de s’inscrire en parsant son cv.
-	Pouvoir renseigner ses informations personnelles et sa recherche d’emploi 
-	Récupérer des offres d’emplois correspondantes via une API extérieure
-	Poster les candidatures sur différents jobBoards
-	Définition de la charte graphique et des Mockups
-	Back-end en PHP et front-end en fram
-	ework VueJS et Vuetify.
-	Base de données en MySQL


Pour le front :

Il faut avoir installé node et npm préalablement.
Le front-end est réalisé avec le framework VueJs.
Nous utilisons également la librairie Vuetify.
Utilisation de Vuex pour centraliser les méthodes et données (store.js)
Utilisation d’un localStorage.

Le membre peut se connecter ou s’inscrire en parsant ou non son cv, puis renseigner son espace profil et sa recherche d’emploi, pour ensuite accéder aux offres d’emplois via l’onglet offres d’emploi.

Après avoir récupéré le dossier, lancez la compilation avec npm run serve.

Si une erreur apparait, supprimez le dossier node-modules, puis faites un npm install.

Réessayez le npm run serve.

Pour le back :

Le back est en Symfony 4.3. 
Il est construit comme une API et sécurisé par un JSON WEB TOKEN.
Il fait appel également à une API extérieure Pôle Emploi.

Pour utiliser l’API externe et ainsi récupérer les offres d’emploi, il faut créer un compte sur Emploi Store-dev, créer une application et renseigner son identifiant et sa clé secrète dans le fichier APIAccesTokenController.

Pour créer l'authentification il faut via la ligne de commande générer deux clés SSH, pour cela il faut rentrer ces trois lignes : 

- mkdir -p config/jwt
- openssl genpkey -out config/jwt/private.pem -aes256 -algorithm rsa -pkeyopt rsa_keygen_bits:4096
- openssl pkey -in config/jwt/private.pem -out config/jwt/public.pem -pubout

Quand un mot de passe vous est demandé rentrer la pass_phrase : TheStrongPassPhraseEver

Afin de pouvoir lancer le back-end Symfony :
- git init
- git remote add origin + lien projet
- git pull origin master
- composer install
- php bin/console doctrine:database:create
- régler .env avec identifiants si nécessaire
- importer le fichier sql dans /Base de données MySQL via PHPMyAdmin
- lancer le serveur avec php bin/console server:run

Le deuxième back, qui contient le parser, est un back en nodeJS.

Il faut donc préalablement avoir installé node et nodemon.

Pour le lancer, il faut aller dans le dossier et faire nodemon index.

Afin d'utiliser le parser, ajouter un dossier qui se nomme "compiled" dans le dossier uploads (dans le dossier resume_parser)

Au parse du cv, il est envoyé dans le dossier upload.

Il est possible de modifier les champs du dictionnary dans node-modules/resume-parser/src afin d’affiner les données parsées.

Dans upload, le dossier compilé contient les données parsées en JSON de chaque cv.
