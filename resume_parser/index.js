const express = require('express');
const cors = require('cors');
const multer = require('multer');
const ResumeParser = require('resume-parser');
const req = require('request');
let crypto = require('crypto');
let path = require('path');

const app = express();

app.use(cors());


const fileFilter =
(req, file, cb) => {
    const allowedTypes = ["application/pdf"];
    if (!allowedTypes.includes(file.mimetype)){
        const error = new Error('Incorrect file');
        error.code = "INCORRECT_FILETYPE";
        return cb(error, false)
    }
    cb(null, true);
}       
var storage = multer.diskStorage({
    destination: './uploads',
    filename: function (req, file, cb) {
      crypto.pseudoRandomBytes(16, function (err, raw) {
        if (err) return cb(err)
  
        cb(null, raw.toString('hex') + path.extname(file.originalname))
      })
    }
});
  
var upload = multer({ storage: storage,  limits: {fileSize: 5000000}, fileFilter });

const port = '5000';

app.post('/upload', upload.single('file'), (req, res, next) => {
    let name = req.file.filename;
    ResumeParser
      .parseResumeFile('./uploads/'+  name, './uploads/compiled')
      .then((response) => {
          res.send(response);
      })
      .catch((err) => {
          res.send('Fichier erroné');
      })
});

app.use((err, req, res, next) => {
    if(err.code === 'INCORRECT_FILETYPE'){
        res.status(422).json({
            error: 'Only PDF are allowed'
        });
        return;
    }
    if (err.code === 'LIMIT_FILE_SIZE'){
        res.status(422).json({
            error: 'Allow file size is 5 Mo'
        });
        return;
    }
});

app.listen(port, () => {
    console.log(`Server is running on port: ${port}`);
});