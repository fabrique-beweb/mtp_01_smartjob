-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mar. 12 nov. 2019 à 09:13
-- Version du serveur :  5.7.24
-- Version de PHP :  7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `smartjob`
--

-- --------------------------------------------------------

--
-- Structure de la table `contract`
--

DROP TABLE IF EXISTS `contract`;
CREATE TABLE IF NOT EXISTS `contract` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `contract`
--

INSERT INTO `contract` (`id`, `name`) VALUES
(1, 'Contrat à durée indéterminée'),
(2, 'Contrat à durée déterminée'),
(3, 'CDD sénior'),
(4, 'Mission intérimaire'),
(5, 'Contrat travail saisonnier'),
(6, 'Profession commerciale'),
(7, 'Franchise'),
(8, 'Profession libérale'),
(9, 'Reprise d\'entreprise'),
(10, 'Contrat travail temporaire insertion'),
(11, 'Contrat durée déterminée insertion'),
(12, 'CDI Intérimaire');

-- --------------------------------------------------------

--
-- Structure de la table `domain`
--

DROP TABLE IF EXISTS `domain`;
CREATE TABLE IF NOT EXISTS `domain` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=111 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `domain`
--

INSERT INTO `domain` (`id`, `name`) VALUES
(1, 'Engins agricoles et forestiers'),
(2, 'Espaces naturels et espaces verts'),
(3, 'Etudes et assistance technique'),
(4, 'Production'),
(5, 'Soins aux animaux'),
(6, 'Arts plastiques'),
(7, 'Céramique'),
(8, 'Décoration'),
(9, 'Fibres et papier'),
(10, 'Instruments de musique'),
(11, 'Métal, verre, bijouterie et horlogerie'),
(12, 'Taxidermie'),
(13, 'Tissu et cuirs'),
(14, 'Assurance'),
(15, 'Banque'),
(16, 'Finance'),
(17, 'Gestion administrative banque et assurances'),
(18, 'Immobilier'),
(19, 'Commerce alimentaire et métiers de bouche'),
(20, 'Commerce non alimentaire et de prestations de confort'),
(21, 'Direction de magasin de détail'),
(22, 'Force de vente'),
(23, 'Grande distribution'),
(24, 'Edition et communication'),
(25, 'Images et sons'),
(26, 'Industries graphiques'),
(27, 'Publicité'),
(28, 'Conception et études'),
(29, 'Conduite et encadrement de chantier - travaux'),
(30, 'Engins de chantier'),
(31, 'Extraction'),
(32, 'Montage de structures'),
(33, 'Second oeuvre'),
(34, 'Travaux et gros oeuvre'),
(35, 'Accueil et promotion touristique'),
(36, 'Animation d\'activités de loisirs'),
(37, 'Conception, commercialisation et vente de produits touristiques'),
(38, 'Gestion et direction'),
(39, 'Personnel d\'étage en hôtellerie'),
(40, 'Production culinaire'),
(41, 'Accueil en hôtellerie'),
(42, 'Service'),
(43, 'Affaires et support technique client'),
(44, 'Conception, recherche, études et développement'),
(45, 'Hygiène Sécurité Environnement -HSE- industriels'),
(46, 'Méthodes et gestion industrielles'),
(47, 'Qualité et analyses industrielles'),
(48, 'Alimentaire'),
(49, 'Bois'),
(50, 'Chimie et pharmacie'),
(51, 'Cuir et textile'),
(52, 'Direction, encadrement et pilotage de fabrication et production industrielles'),
(53, 'Electronique et électricité'),
(54, 'Energie'),
(55, 'Matériaux de construction, céramique et verre'),
(56, 'Mécanique, travail des métaux et outillage'),
(57, 'Papier et carton'),
(58, 'Plastique, caoutchouc'),
(59, 'Préparation et conditionnement'),
(60, 'Traitements thermiques et traitements de surfaces'),
(61, 'Encadrement'),
(62, 'Entretien technique'),
(63, 'Equipements de production, équipements collectifs'),
(64, 'Equipements domestiques et informatique'),
(65, 'Travaux d\'accès difficile'),
(66, 'Véhicules, engins, aéronefs'),
(67, 'Praticiens médicaux'),
(68, 'Praticiens médico-techniques'),
(69, 'Professionnels médico-techniques'),
(70, 'Rééducation et appareillage'),
(71, 'Soins paramédicaux'),
(72, 'Accompagnement de la personne'),
(73, 'Action sociale, socio-éducative et socio-culturelle'),
(74, 'Aide à la vie quotidienne'),
(75, 'Conception et mise en oeuvre des politiques publiques'),
(76, 'Contrôle public'),
(77, 'Culture et gestion documentaire'),
(78, 'Défense, sécurité publique et secours'),
(79, 'Développement territorial et emploi'),
(80, 'Droit'),
(81, 'Formation initiale et continue'),
(82, 'Nettoyage et propreté industriels'),
(83, 'Propreté et environnement urbain'),
(84, 'Recherche'),
(85, 'Sécurité privée'),
(86, 'Services funéraires'),
(87, 'Animation de spectacles'),
(88, 'Artistes - interprètes du spectacle'),
(89, 'Conception et production de spectacles'),
(90, 'Sport professionnel'),
(91, 'Techniciens du spectacle'),
(92, 'Achats'),
(93, 'Comptabilité et gestion'),
(94, 'Direction d\'entreprise'),
(95, 'Organisation et études'),
(96, 'Ressources humaines'),
(97, 'Secrétariat et assistance'),
(98, 'Stratégie commerciale, marketing et supervision des ventes'),
(99, 'Systèmes d\'information et de télécommunication'),
(100, 'Magasinage, manutention des charges et déménagement'),
(101, 'Organisation de la circulation des marchandises'),
(102, 'Personnel d\'encadrement de la logistique'),
(103, 'Personnel navigant du transport aérien'),
(104, 'Personnel sédentaire du transport aérien'),
(105, 'Personnel navigant du transport maritime et fluvial'),
(106, 'Personnel sédentaire du transport maritime et fluvial'),
(107, 'Personnel de conduite du transport routier'),
(108, 'Personnel d\'encadrement du transport routier'),
(109, 'Personnel navigant du transport terrestre'),
(110, 'Personnel sédentaire du transport ferroviaire et réseau filo guidé');

-- --------------------------------------------------------

--
-- Structure de la table `experience`
--

DROP TABLE IF EXISTS `experience`;
CREATE TABLE IF NOT EXISTS `experience` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_590C103A76ED395` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `formation`
--

DROP TABLE IF EXISTS `formation`;
CREATE TABLE IF NOT EXISTS `formation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `level` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `school` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_404021BFA76ED395` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `hobbie`
--

DROP TABLE IF EXISTS `hobbie`;
CREATE TABLE IF NOT EXISTS `hobbie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_1D9CA9F7A76ED395` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `part_time`
--

DROP TABLE IF EXISTS `part_time`;
CREATE TABLE IF NOT EXISTS `part_time` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `part_time`
--

INSERT INTO `part_time` (`id`, `name`) VALUES
(1, 'Temps plein'),
(2, 'Temps partiel');

-- --------------------------------------------------------

--
-- Structure de la table `pay`
--

DROP TABLE IF EXISTS `pay`;
CREATE TABLE IF NOT EXISTS `pay` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pay_range` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `pay`
--

INSERT INTO `pay` (`id`, `pay_range`) VALUES
(1, '10 000€ et plus'),
(2, '20 000€ et plus'),
(3, '30 000€ et plus'),
(4, '40 000€ et plus'),
(5, '50 000€ et plus'),
(6, '60 000€ et plus'),
(7, '70 000€ et plus'),
(8, '80 000€ et plus'),
(9, '90 000€ et plus'),
(10, '100 000€ et plus'),
(11, '200 000€ et plus');

-- --------------------------------------------------------

--
-- Structure de la table `research`
--

DROP TABLE IF EXISTS `research`;
CREATE TABLE IF NOT EXISTS `research` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pay_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` json DEFAULT NULL,
  `location` json DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_57EB50C2918501AB` (`pay_id`),
  KEY `IDX_57EB50C2A76ED395` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `research_contract`
--

DROP TABLE IF EXISTS `research_contract`;
CREATE TABLE IF NOT EXISTS `research_contract` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `research_id` int(11) NOT NULL,
  `contract_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_14747F937909E1ED` (`research_id`),
  KEY `IDX_14747F932576E0FD` (`contract_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `research_domain`
--

DROP TABLE IF EXISTS `research_domain`;
CREATE TABLE IF NOT EXISTS `research_domain` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `research_id` int(11) NOT NULL,
  `domain_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_48BE087909E1ED` (`research_id`),
  KEY `IDX_48BE08115F0EE5` (`domain_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `research_part_time`
--

DROP TABLE IF EXISTS `research_part_time`;
CREATE TABLE IF NOT EXISTS `research_part_time` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `research_id` int(11) NOT NULL,
  `part_time_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_6E7036317909E1ED` (`research_id`),
  KEY `IDX_6E703631DEB62A30` (`part_time_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `skill`
--

DROP TABLE IF EXISTS `skill`;
CREATE TABLE IF NOT EXISTS `skill` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_5E3DE477A76ED395` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` json NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cv` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `diploma_level` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `experience_years` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `email`, `roles`, `password`, `first_name`, `last_name`, `avatar`, `cv`, `diploma_level`, `experience_years`) VALUES
(1, 'yannick@yannick.com', '[]', '$argon2id$v=19$m=65536,t=4,p=1$SVR1WlkuTEZMblVXQjkwLw$futgzG4iPmkUrl8FCmsu9hfxxStbPNkBnZ7ZgMzOlcQ', 'Yann', 'Maffre', 'default.jpeg', NULL, 'bac+4', '5 ans');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `experience`
--
ALTER TABLE `experience`
  ADD CONSTRAINT `FK_590C103A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `formation`
--
ALTER TABLE `formation`
  ADD CONSTRAINT `FK_404021BFA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `hobbie`
--
ALTER TABLE `hobbie`
  ADD CONSTRAINT `FK_1D9CA9F7A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `research`
--
ALTER TABLE `research`
  ADD CONSTRAINT `FK_57EB50C2918501AB` FOREIGN KEY (`pay_id`) REFERENCES `pay` (`id`),
  ADD CONSTRAINT `FK_57EB50C2A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `research_contract`
--
ALTER TABLE `research_contract`
  ADD CONSTRAINT `FK_14747F932576E0FD` FOREIGN KEY (`contract_id`) REFERENCES `contract` (`id`),
  ADD CONSTRAINT `FK_14747F937909E1ED` FOREIGN KEY (`research_id`) REFERENCES `research` (`id`);

--
-- Contraintes pour la table `research_domain`
--
ALTER TABLE `research_domain`
  ADD CONSTRAINT `FK_48BE08115F0EE5` FOREIGN KEY (`domain_id`) REFERENCES `domain` (`id`),
  ADD CONSTRAINT `FK_48BE087909E1ED` FOREIGN KEY (`research_id`) REFERENCES `research` (`id`);

--
-- Contraintes pour la table `research_part_time`
--
ALTER TABLE `research_part_time`
  ADD CONSTRAINT `FK_6E7036317909E1ED` FOREIGN KEY (`research_id`) REFERENCES `research` (`id`),
  ADD CONSTRAINT `FK_6E703631DEB62A30` FOREIGN KEY (`part_time_id`) REFERENCES `part_time` (`id`);

--
-- Contraintes pour la table `skill`
--
ALTER TABLE `skill`
  ADD CONSTRAINT `FK_5E3DE477A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
