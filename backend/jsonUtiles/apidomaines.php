<script>
var jsonDomains = [
    {
      "code": "A11",
      "name": "Engins agricoles et forestiers"
    },
    {
      "code": "A12",
      "name": "Espaces naturels et espaces verts"
    },
    {
      "code": "A13",
      "name": "Etudes et assistance technique"
    },
    {
      "code": "A14",
      "name": "Production"
    },
    {
      "code": "A15",
      "name": "Soins aux animaux"
    },
    {
      "code": "B11",
      "name": "Arts plastiques"
    },
    {
      "code": "B12",
      "name": "Céramique"
    },
    {
      "code": "B13",
      "name": "Décoration"
    },
    {
      "code": "B14",
      "name": "Fibres et papier"
    },
    {
      "code": "B15",
      "name": "Instruments de musique"
    },
    {
      "code": "B16",
      "name": "Métal, verre, bijouterie et horlogerie"
    },
    {
      "code": "B17",
      "name": "Taxidermie"
    },
    {
      "code": "B18",
      "name": "Tissu et cuirs"
    },
    {
      "code": "C11",
      "name": "Assurance"
    },
    {
      "code": "C12",
      "name": "Banque"
    },
    {
      "code": "C13",
      "name": "Finance"
    },
    {
      "code": "C14",
      "name": "Gestion administrative banque et assurances"
    },
    {
      "code": "C15",
      "name": "Immobilier"
    },
    {
      "code": "D11",
      "name": "Commerce alimentaire et métiers de bouche"
    },
    {
      "code": "D12",
      "name": "Commerce non alimentaire et de prestations de confort"
    },
    {
      "code": "D13",
      "name": "Direction de magasin de détail"
    },
    {
      "code": "D14",
      "name": "Force de vente"
    },
    {
      "code": "D15",
      "name": "Grande distribution"
    },
    {
      "code": "E11",
      "name": "Edition et communication"
    },
    {
      "code": "E12",
      "name": "Images et sons"
    },
    {
      "code": "E13",
      "name": "Industries graphiques"
    },
    {
      "code": "E14",
      "name": "Publicité"
    },
    {
      "code": "F11",
      "name": "Conception et études"
    },
    {
      "code": "F12",
      "name": "Conduite et encadrement de chantier - travaux"
    },
    {
      "code": "F13",
      "name": "Engins de chantier"
    },
    {
      "code": "F14",
      "name": "Extraction"
    },
    {
      "code": "F15",
      "name": "Montage de structures"
    },
    {
      "code": "F16",
      "name": "Second oeuvre"
    },
    {
      "code": "F17",
      "name": "Travaux et gros oeuvre"
    },
    {
      "code": "G11",
      "name": "Accueil et promotion touristique"
    },
    {
      "code": "G12",
      "name": "Animation d'activités de loisirs"
    },
    {
      "code": "G13",
      "name": "Conception, commercialisation et vente de produits touristiques"
    },
    {
      "code": "G14",
      "name": "Gestion et direction"
    },
    {
      "code": "G15",
      "name": "Personnel d'étage en hôtellerie"
    },
    {
      "code": "G16",
      "name": "Production culinaire"
    },
    {
      "code": "G17",
      "name": "Accueil en hôtellerie"
    },
    {
      "code": "G18",
      "name": "Service"
    },
    {
      "code": "H11",
      "name": "Affaires et support technique client"
    },
    {
      "code": "H12",
      "name": "Conception, recherche, études et développement"
    },
    {
      "code": "H13",
      "name": "Hygiène Sécurité Environnement -HSE- industriels"
    },
    {
      "code": "H14",
      "name": "Méthodes et gestion industrielles"
    },
    {
      "code": "H15",
      "name": "Qualité et analyses industrielles"
    },
    {
      "code": "H21",
      "name": "Alimentaire"
    },
    {
      "code": "H22",
      "name": "Bois"
    },
    {
      "code": "H23",
      "name": "Chimie et pharmacie"
    },
    {
      "code": "H24",
      "name": "Cuir et textile"
    },
    {
      "code": "H25",
      "name": "Direction, encadrement et pilotage de fabrication et production industrielles"
    },
    {
      "code": "H26",
      "name": "Electronique et électricité"
    },
    {
      "code": "H27",
      "name": "Energie"
    },
    {
      "code": "H28",
      "name": "Matériaux de construction, céramique et verre"
    },
    {
      "code": "H29",
      "name": "Mécanique, travail des métaux et outillage"
    },
    {
      "code": "H31",
      "name": "Papier et carton"
    },
    {
      "code": "H32",
      "name": "Plastique, caoutchouc"
    },
    {
      "code": "H33",
      "name": "Préparation et conditionnement"
    },
    {
      "code": "H34",
      "name": "Traitements thermiques et traitements de surfaces"
    },
    {
      "code": "I11",
      "name": "Encadrement"
    },
    {
      "code": "I12",
      "name": "Entretien technique"
    },
    {
      "code": "I13",
      "name": "Equipements de production, équipements collectifs"
    },
    {
      "code": "I14",
      "name": "Equipements domestiques et informatique"
    },
    {
      "code": "I15",
      "name": "Travaux d'accès difficile"
    },
    {
      "code": "I16",
      "name": "Véhicules, engins, aéronefs"
    },
    {
      "code": "J11",
      "name": "Praticiens médicaux"
    },
    {
      "code": "J12",
      "name": "Praticiens médico-techniques"
    },
    {
      "code": "J13",
      "name": "Professionnels médico-techniques"
    },
    {
      "code": "J14",
      "name": "Rééducation et appareillage"
    },
    {
      "code": "J15",
      "name": "Soins paramédicaux"
    },
    {
      "code": "K11",
      "name": "Accompagnement de la personne"
    },
    {
      "code": "K12",
      "name": "Action sociale, socio-éducative et socio-culturelle"
    },
    {
      "code": "K13",
      "name": "Aide à la vie quotidienne"
    },
    {
      "code": "K14",
      "name": "Conception et mise en oeuvre des politiques publiques"
    },
    {
      "code": "K15",
      "name": "Contrôle public"
    },
    {
      "code": "K16",
      "name": "Culture et gestion documentaire"
    },
    {
      "code": "K17",
      "name": "Défense, sécurité publique et secours"
    },
    {
      "code": "K18",
      "name": "Développement territorial et emploi"
    },
    {
      "code": "K19",
      "name": "Droit"
    },
    {
      "code": "K21",
      "name": "Formation initiale et continue"
    },
    {
      "code": "K22",
      "name": "Nettoyage et propreté industriels"
    },
    {
      "code": "K23",
      "name": "Propreté et environnement urbain"
    },
    {
      "code": "K24",
      "name": "Recherche"
    },
    {
      "code": "K25",
      "name": "Sécurité privée"
    },
    {
      "code": "K26",
      "name": "Services funéraires"
    },
    {
      "code": "L11",
      "name": "Animation de spectacles"
    },
    {
      "code": "L12",
      "name": "Artistes - interprètes du spectacle"
    },
    {
      "code": "L13",
      "name": "Conception et production de spectacles"
    },
    {
      "code": "L14",
      "name": "Sport professionnel"
    },
    {
      "code": "L15",
      "name": "Techniciens du spectacle"
    },
    {
      "code": "M11",
      "name": "Achats"
    },
    {
      "code": "M12",
      "name": "Comptabilité et gestion"
    },
    {
      "code": "M13",
      "name": "Direction d'entreprise"
    },
    {
      "code": "M14",
      "name": "Organisation et études"
    },
    {
      "code": "M15",
      "name": "Ressources humaines"
    },
    {
      "code": "M16",
      "name": "Secrétariat et assistance"
    },
    {
      "code": "M17",
      "name": "Stratégie commerciale, marketing et supervision des ventes"
    },
    {
      "code": "M18",
      "name": "Systèmes d'information et de télécommunication"
    },
    {
      "code": "N11",
      "name": "Magasinage, manutention des charges et déménagement"
    },
    {
      "code": "N12",
      "name": "Organisation de la circulation des marchandises"
    },
    {
      "code": "N13",
      "name": "Personnel d'encadrement de la logistique"
    },
    {
      "code": "N21",
      "name": "Personnel navigant du transport aérien"
    },
    {
      "code": "N22",
      "name": "Personnel sédentaire du transport aérien"
    },
    {
      "code": "N31",
      "name": "Personnel navigant du transport maritime et fluvial"
    },
    {
      "code": "N32",
      "name": "Personnel sédentaire du transport maritime et fluvial"
    },
    {
      "code": "N41",
      "name": "Personnel de conduite du transport routier"
    },
    {
      "code": "N42",
      "name": "Personnel d'encadrement du transport routier"
    },
    {
      "code": "N43",
      "name": "Personnel navigant du transport terrestre"
    },
    {
      "code": "N44",
      "name": "Personnel sédentaire du transport ferroviaire et réseau filo guidé"
    }
  ];

  
 
</script>
