<?php

namespace App\Repository;

use App\Entity\PartTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method PartTime|null find($id, $lockMode = null, $lockVersion = null)
 * @method PartTime|null findOneBy(array $criteria, array $orderBy = null)
 * @method PartTime[]    findAll()
 * @method PartTime[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PartTimeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PartTime::class);
    }

    // /**
    //  * @return PartTime[] Returns an array of PartTime objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PartTime
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
