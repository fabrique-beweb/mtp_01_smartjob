<?php

namespace App\Repository;

use App\Entity\ResearchContract;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ResearchContract|null find($id, $lockMode = null, $lockVersion = null)
 * @method ResearchContract|null findOneBy(array $criteria, array $orderBy = null)
 * @method ResearchContract[]    findAll()
 * @method ResearchContract[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ResearchContractRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ResearchContract::class);
    }

    // /**
    //  * @return ResearchContract[] Returns an array of ResearchContract objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ResearchContract
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
