<?php

namespace App\Repository;

use App\Entity\ResearchPartTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ResearchPartTime|null find($id, $lockMode = null, $lockVersion = null)
 * @method ResearchPartTime|null findOneBy(array $criteria, array $orderBy = null)
 * @method ResearchPartTime[]    findAll()
 * @method ResearchPartTime[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ResearchPartTimeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ResearchPartTime::class);
    }

    // /**
    //  * @return ResearchPartTime[] Returns an array of ResearchPartTime objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ResearchPartTime
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
