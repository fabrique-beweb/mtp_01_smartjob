<?php
namespace App\Services;


/**
 * This class is used to convert the private props of en entity to public of this class
 * Class ObjectBuilder
 * @package App\Services
 */
class ObjectBuilder
{
    //Use to convert user entity to json
    public function setMember( $member )
    {
        $this->id = $member->getId();
        $this->email = $member->getEmail();
        $this->firstName = $member->getFirstName();
        $this->lastName = $member->getLastName();
        $this->avatar = $member->getAvatar();
        $this->cv = $member->getCv();
        $this->experienceYears = $member->getExperienceYears();
        $this->diplomaLevel = $member->getDiplomaLevel();
        return $this;
    }

    //Use to convert hobbie entity to json
    public function setHobbie( $hobbie )
    {
        $this->id = $hobbie->getId();
        $this->name = $hobbie->getName();
        $this->user = $hobbie->getUser()->getId();
        return $this;
    }

    //Use to convert skill entity to json
    public function setSkill( $skill )
    {
        $this->id = $skill->getId();
        $this->name = $skill->getName();
        $this->user = $skill->getUser()->getId();
        return $this;
    }

    //Use to convert formation entity to json
    public function setFormation( $formation )
    {
        $this->id = $formation->getId();
        $this->title = $formation->getTitle();
        $this->level = $formation->getLevel();
        $this->school = $formation->getSchool();
        $this->location = $formation->getLocation();
        $this->description = $formation->getDescription();
        $this->startDate = $formation->getStartDate();
        $this->endDate = $formation->getEndDate();
        $this->user = $formation->getUser()->getId();
        return $this;
    }

    //Use to convert experience entity to json
    public function setExperience( $experience )
    {
        $this->id = $experience->getId();
        $this->title = $experience->getTitle();
        $this->company = $experience->getCompany();
        $this->location = $experience->getLocation();
        $this->description = $experience->getDescription();
        $this->startDate = $experience->getStartDate();
        $this->endDate = $experience->getEndDate();
        $this->user = $experience->getUser()->getId();
        return $this;
    }

    //Use to convert pay entity to json
    public function setPay( $pay )
    {
        $this->id = $pay->getId();
        $this->name = $pay->getPayRange();
        return $this;
    }

    //Use to convert domain entity to json
    public function setDomain( $domain )
    {
        $this->id = $domain->getId();
        $this->name = $domain->getName();
        return $this;
    }

    //Use to convert partTime entity to json
    public function setPartTime( $partTime )
    {
        $this->id = $partTime->getId();
        $this->name = $partTime->getName();
        return $this;
    }

    //Use to convert contract entity to json
    public function setContract( $contract )
    {
        $this->id = $contract->getId();
        $this->name = $contract->getName();
        return $this;
    }

    //Use to convert research entity to json
    public function setResearch( $research )
    {
        $this->id = $research->getId();
        $this->title = $research->getTitle();
        $this->location = $research->getLocation();
        $this->pay = [ "id" => $research->getPay()->getId(), "payRange" => $research->getPay()->getPayRange() ];
        $this->user = [ "id" => $research->getUser()->getId(), "email" => $research->getUser()->getEmail() ];
        $this->researchDomains = [];
        for( $i = 0; $i < count( $research->getResearchDomains() ); $i++ )
        {
            $this->researchDomains[ $i ]= $this->setResearchDomainsForResearch( $research->getResearchDomains()[ $i ] );
        }
        $this->researchContracts = [];
        for( $i = 0; $i < count( $research->getResearchContracts() ); $i++ )
        {
            $this->researchContracts[ $i ]= $this->setResearchContractsForResearch( $research->getResearchContracts()[ $i ] );
        }
        $this->researchPartTimes = [];
        for( $i = 0; $i < count( $research->getResearchPartTimes() ); $i++ )
        {
            $this->researchPartTimes[ $i ]= $this->setResearchPartTimesForResearch( $research->getResearchPartTimes()[ $i ] );
        }
        return $this;
    }

    //Serialize researchDomains for research
    private function setResearchDomainsForResearch( $researchDomains )
    {
        return $domains = [ "id" => $researchDomains->getDomain()->getId(), "name" => $researchDomains->getDomain()->getName() ];
    }
    //Serialize researchContracts for research
    private function setResearchContractsForResearch( $researchContracts )
    {
        return $contracts = [ "id" => $researchContracts->getContract()->getId(), "name" => $researchContracts->getContract()->getName() ];
    }
    //Serialize researchPartTime for research
    private function setResearchPartTimesForResearch( $researchPartTimes )
    {
        return $partTimes = [ "id" => $researchPartTimes->getPartTime()->getId(), "name" => $researchPartTimes->getPartTime()->getName() ];
    }
}