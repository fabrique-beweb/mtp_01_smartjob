<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ResearchRepository")
 */
class Research
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $title;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $location;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Pay", inversedBy="researches")
     * @ORM\JoinColumn(nullable=false)
     */
    private $pay;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="researches")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ResearchDomain", mappedBy="research")
     */
    private $researchDomains;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ResearchPartTime", mappedBy="research")
     */
    private $researchPartTimes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ResearchContract", mappedBy="research")
     */
    private $researchContracts;

    public function __construct()
    {
        $this->researchDomains = new ArrayCollection();
        $this->researchPartTimes = new ArrayCollection();
        $this->researchContracts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?array
    {
        $title[] = $this->title;
        return array_unique($title);
    }

    public function setTitle(?array $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getLocation(): ?array
    {
        $location[] = $this->location;
        return array_unique($location);
    }

    public function setLocation(?array $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getPay(): ?Pay
    {
        return $this->pay;
    }

    public function setPay(?Pay $pay): self
    {
        $this->pay = $pay;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|ResearchDomain[]
     */
    public function getResearchDomains(): Collection
    {
        return $this->researchDomains;
    }

    public function addResearchDomain(ResearchDomain $researchDomain): self
    {
        if (!$this->researchDomains->contains($researchDomain)) {
            $this->researchDomains[] = $researchDomain;
            $researchDomain->setResearch($this);
        }

        return $this;
    }

    public function removeResearchDomain(ResearchDomain $researchDomain): self
    {
        if ($this->researchDomains->contains($researchDomain)) {
            $this->researchDomains->removeElement($researchDomain);
            // set the owning side to null (unless already changed)
            if ($researchDomain->getResearch() === $this) {
                $researchDomain->setResearch(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ResearchPartTime[]
     */
    public function getResearchPartTimes(): Collection
    {
        return $this->researchPartTimes;
    }

    public function addResearchPartTime(ResearchPartTime $researchPartTime): self
    {
        if (!$this->researchPartTimes->contains($researchPartTime)) {
            $this->researchPartTimes[] = $researchPartTime;
            $researchPartTime->setResearch($this);
        }

        return $this;
    }

    public function removeResearchPartTime(ResearchPartTime $researchPartTime): self
    {
        if ($this->researchPartTimes->contains($researchPartTime)) {
            $this->researchPartTimes->removeElement($researchPartTime);
            // set the owning side to null (unless already changed)
            if ($researchPartTime->getResearch() === $this) {
                $researchPartTime->setResearch(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ResearchContract[]
     */
    public function getResearchContracts(): Collection
    {
        return $this->researchContracts;
    }

    public function addResearchContract(ResearchContract $researchContract): self
    {
        if (!$this->researchContracts->contains($researchContract)) {
            $this->researchContracts[] = $researchContract;
            $researchContract->setResearch($this);
        }

        return $this;
    }

    public function removeResearchContract(ResearchContract $researchContract): self
    {
        if ($this->researchContracts->contains($researchContract)) {
            $this->researchContracts->removeElement($researchContract);
            // set the owning side to null (unless already changed)
            if ($researchContract->getResearch() === $this) {
                $researchContract->setResearch(null);
            }
        }

        return $this;
    }
}
