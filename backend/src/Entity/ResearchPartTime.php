<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ResearchPartTimeRepository")
 */
class ResearchPartTime
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Research", inversedBy="researchPartTimes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $research;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PartTime", inversedBy="researchPartTimes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $partTime;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getResearch(): ?Research
    {
        return $this->research;
    }

    public function setResearch(?Research $research): self
    {
        $this->research = $research;

        return $this;
    }

    public function getPartTime(): ?PartTime
    {
        return $this->partTime;
    }

    public function setPartTime(?PartTime $partTime): self
    {
        $this->partTime = $partTime;

        return $this;
    }
}
