<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PartTimeRepository")
 */
class PartTime
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ResearchPartTime", mappedBy="partTime")
     */
    private $researchPartTimes;

    public function __construct()
    {
        $this->researchPartTimes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|ResearchPartTime[]
     */
    public function getResearchPartTimes(): Collection
    {
        return $this->researchPartTimes;
    }

    public function addResearchPartTime(ResearchPartTime $researchPartTime): self
    {
        if (!$this->researchPartTimes->contains($researchPartTime)) {
            $this->researchPartTimes[] = $researchPartTime;
            $researchPartTime->setPartTime($this);
        }

        return $this;
    }

    public function removeResearchPartTime(ResearchPartTime $researchPartTime): self
    {
        if ($this->researchPartTimes->contains($researchPartTime)) {
            $this->researchPartTimes->removeElement($researchPartTime);
            // set the owning side to null (unless already changed)
            if ($researchPartTime->getPartTime() === $this) {
                $researchPartTime->setPartTime(null);
            }
        }

        return $this;
    }
}
