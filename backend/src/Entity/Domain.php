<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DomainRepository")
 */
class Domain
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ResearchDomain", mappedBy="domain")
     */
    private $researchDomains;

    public function __construct()
    {
        $this->researchDomains = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|ResearchDomain[]
     */
    public function getResearchDomains(): Collection
    {
        return $this->researchDomains;
    }

    public function addResearchDomain(ResearchDomain $researchDomain): self
    {
        if (!$this->researchDomains->contains($researchDomain)) {
            $this->researchDomains[] = $researchDomain;
            $researchDomain->setDomain($this);
        }

        return $this;
    }

    public function removeResearchDomain(ResearchDomain $researchDomain): self
    {
        if ($this->researchDomains->contains($researchDomain)) {
            $this->researchDomains->removeElement($researchDomain);
            // set the owning side to null (unless already changed)
            if ($researchDomain->getDomain() === $this) {
                $researchDomain->setDomain(null);
            }
        }

        return $this;
    }
}
