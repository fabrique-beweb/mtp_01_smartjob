<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ContractRepository")
 */
class Contract
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ResearchContract", mappedBy="contract")
     */
    private $researchContracts;

    public function __construct()
    {
        $this->researchContracts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|ResearchContract[]
     */
    public function getResearchContracts(): Collection
    {
        return $this->researchContracts;
    }

    public function addResearchContract(ResearchContract $researchContract): self
    {
        if (!$this->researchContracts->contains($researchContract)) {
            $this->researchContracts[] = $researchContract;
            $researchContract->setContract($this);
        }

        return $this;
    }

    public function removeResearchContract(ResearchContract $researchContract): self
    {
        if ($this->researchContracts->contains($researchContract)) {
            $this->researchContracts->removeElement($researchContract);
            // set the owning side to null (unless already changed)
            if ($researchContract->getContract() === $this) {
                $researchContract->setContract(null);
            }
        }

        return $this;
    }
}
