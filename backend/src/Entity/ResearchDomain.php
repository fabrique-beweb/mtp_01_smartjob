<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ResearchDomainRepository")
 */
class ResearchDomain
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Research", inversedBy="researchDomains")
     * @ORM\JoinColumn(nullable=false)
     */
    private $research;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Domain", inversedBy="researchDomains")
     * @ORM\JoinColumn(nullable=false)
     */
    private $domain;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getResearch(): ?Research
    {
        return $this->research;
    }

    public function setResearch(?Research $research): self
    {
        $this->research = $research;

        return $this;
    }

    public function getDomain(): ?Domain
    {
        return $this->domain;
    }

    public function setDomain(?Domain $domain): self
    {
        $this->domain = $domain;

        return $this;
    }
}
