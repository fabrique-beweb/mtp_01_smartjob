<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Domain;
use App\Entity\User;
use App\Entity\ResearchDomain;
use App\Services\ObjectBuilder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class ApiDomainController
 * @package App\Controller
 * @Route("/api", name="api.")
 */
class ApiDomainController extends AbstractController
{
    /**
     * @Route("/domain/{id?}", name="api.domain", methods={"GET", "PUT", "POST", "DELETE"})
     * @param $id
     * @param Request $request
     * @return JsonResponse
     */
    public function apiDomain($id, Request $request)
    {
        if( !$id )
        {
            if( $request->isMethod( 'GET' ))
            {
                //Get all domains entries
                $domain = $this->getDoctrine()->getRepository(Domain::class )->findAll();
                //Init empty domain list array
                $domainList = [];
                //Loop to convert all private props to public props
                for( $i = 0; $i < count( $domain ); $i++ ) {
                    //Init new object
                    $domainList[ $i ] = new ObjectBuilder();
                    //Hydrate object
                    $domainList[ $i ]->setDomain( $domain[ $i ] );
                }
                //Serialize object to json
                return new JsonResponse($domainList);

            } elseif ( $request->isMethod( 'POST' ))
            {
                //Get json content from the post
                $data = json_decode( $request->getContent(), true );

                //verifie que le domainType n'est pas déja present dans la table 
                $Alldomain = $this->getDoctrine()->getRepository(Domain::class )->findAll();
                for ( $j=0; $j < count( $Alldomain ); $j++ )
                {
                    if( $data[ "name" ] === $Alldomain[ $j ]->getName() )
                    {
                        return $this->json( [
                            'response' =>  'le nom de domaine '.$Alldomain[$j]->getName().' existe déjà dans la table domain'
                            ] );
                    }
                }

                //Init a new domain object
                $domain = new Domain;

                //hydrate object
                $domain->setName( $data[ 'name' ] );

                //Entity manager
                $em = $this->getDoctrine()->getManager();
                //Persist entity
                $em->persist($domain);
                //Insert to database
                $em->flush();
                //Send back 201 status
                return $this->json([
                    'response' =>  '201'
                ]);
            } elseif ( $request->isMethod( 'PUT' ) )
            {
                //Get json from the PUT and decode it
                $data = json_decode( $request->getContent(), true );
                //Search domain by the given ID
                $domain = $this->getDoctrine()->getRepository( Domain::class )->find( $data[ 'id' ] );

                if( $domain ) // If the domain at the given id exist
                {
                    try{
                        $domain->setName( $data[ 'name' ] );
                        //Entity manager
                        $em = $this->getDoctrine()->getManager();
                        //Update the given domain
                        $em->merge( $domain );
                        $em->flush();
                        //Return code 200
                        return $this->json([
                            'response' => 200
                        ]);
                    } catch( \Exception $e ){
                        return $this->json( [
                            "response" => "le domaine : ". $data['name']." existe deja",
                            "error" => $e->getMessage()
                        ] );
                    }

                } else { //If the domain doesn't exist return response
                    return $this->json([
                        'response' => 'Le domaine demandé n\'existe pas'
                    ]);
                }
            }

        } elseif ( $id )
        {
            if ( $request->isMethod( 'GET' ) )
            {
                $domain = $this->getDoctrine()->getRepository(domain::class )->find( $id );

                $temp = new ObjectBuilder();

                $temp->setDomain( $domain );

                return new JsonResponse( $temp );

            } elseif ( $request->isMethod( 'DELETE' ) )
            {

                $research_domain = $this->getDoctrine()->getRepository( ResearchDomain::class )->findBy( [ 'domain' => $id ] );
                $domain = $this->getDoctrine()->getRepository( domain::class )->find( $id );

                if ( $research_domain ) {

                    return new JsonResponse( ['response' => 'Attention, ce domaine a déja été choisi dans les recherches d\'utilisateurs, vous ne pouvez pas le supprimer']);
                } else
                {
                    if( $domain ) // If the domain at the given id exist
                    {
                        //Entity manager
                        $em = $this->getDoctrine()->getManager();
                        //Remove the given domain
                        $em->remove( $domain );
                        $em->flush();
                        //Return code 200
                        return $this->json( [
                            'response' => 200
                        ] );
                    } else { //If the domain doesn't exist return response
                        return $this->json( [
                            'response' => 'Le domaine demandé n\'existe pas'
                        ] );
                    }
                }
            }
        } else
            {
            return $this->json( [
                'response' => 500
            ] );
        }
    }
}