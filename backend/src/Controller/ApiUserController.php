<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;
use App\Services\ObjectBuilder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use App\Service\FileUploader;

/**
 * Class ApiUserController
 * @package App\Controller
 * @Route("/api", name="api.")
 */
class ApiUserController extends AbstractController
{
    /**
     * @Route("/user/{id?}", name="api.user", methods={"GET", "PUT", "POST", "DELETE"})
     * @param $id
     * @param Request $request
     * @return JsonResponse
     */
    public function apiUser($id, Request $request, UserInterface $user, UserPasswordEncoderInterface $encoder, FileUploader $fileUploader)
    {
        if ( $id )
        {
            if ( $request->isMethod( 'GET' ) )
            {   
                //$id = base64_decode($id);
                $idUser = $user->getId();
                $member = $this->getDoctrine()->getRepository(User::class )->find( $idUser );

                $temp = new ObjectBuilder();

                $temp->setMember( $member );

                $json = new JsonResponse( $temp );

                return $json;

            } elseif ( $request->isMethod( 'DELETE' ) )
            {
                //Search formation by the given ID
                $member = $this->getDoctrine()->getRepository( User::class )->find( $id );

                if ( !$member ) {
                         //If the user doesn't exist return response
                    return $this->json([
                        'response' => 'L\'utilisateur demandé n\'existe pas'
                    ]);
                } else {

                   // Delete all from userId à faire

                    return $this->json([
                        'response' => 'pas encore fait'
                    ]);
                }
            }
        }elseif(!$id){
            if ( $request->isMethod( 'GET' ) )
            {   
                //$id = base64_decode($id);
                $idUser = $user->getId();
                $member = $this->getDoctrine()->getRepository(User::class )->find( $idUser );

                $temp = new ObjectBuilder();

                $temp->setMember( $member );

                $json = new JsonResponse( $temp );

                return $json;
            }elseif ( $request->isMethod( 'POST' ) ){
                $myfile = $request->files->get('myfile');
                //$myfile = $request->request->get('file');

                $idUser = $user->getId();
                $member = $this->getDoctrine()->getRepository(User::class )->find( $idUser );

                
                if($myfile){
                    $avatarFile = $myfile;
                    if ($avatarFile) {
                        $avatarFileName = $fileUploader->upload($avatarFile);
                        
                        //Entity manager
                        $em = $this->getDoctrine()->getManager();
                        $member->setAvatar($avatarFileName);
                        //Update the given contract
                        $em->merge( $member );
                        $em->flush();
                        return $this->json([
                            'response' => 'image uploadé'
                        ]);
                    } else {
                        return $this->json([
                            'response' => 'fichier reçu mais problème upload'
                        ]);
                    }
                }else{
                    return $this->json([
                            'response' => 'pas de fichier reçu'
                        ]);
                }

            }
            elseif ( $request->isMethod( 'PUT' ) ){
                //Get json from the PUT and decode it
                $data = json_decode( $request->getContent(), true );
                //$myfile = $request->files->get('file');
                
               
                //Search user by the given ID
                $idUser = $user->getId();
                $member = $this->getDoctrine()->getRepository(User::class )->find( $idUser );

                if( $member ) // If the member at the given id exist
                {
                    try{
                        //verifie que l'email n'est pas deja present dans la table user
                        $AllUsers = $this->getDoctrine()->getRepository(User::class )->findAll();
                        for ($j=0; $j<count($AllUsers); $j++){
                            if(@$data[ 'email' ] == $AllUsers[$j]->getEmail() && @$data[ 'email' ]!== $user->getEmail() ){
                                return $this->json([
                                    'response' =>  'l\'email '.$AllUsers[$j]->getEmail().' existe déjà'
                                    ]);
                            }
                        }
                        
                        if(@$data[ 'email' ]){
                            $member->setEmail( $data[ 'email' ] );
                        }
                        if(@$data[ 'password' ]){
                            $plainPassword = $data[ 'password' ];
                            $member->setPassword( $encoder->encodePassword( $user, $plainPassword ) );
                        }
                        if(@$data[ 'firstname' ]){
                            $member->setFirstName( $data[ 'firstname' ]);
                        }
                        if(@$data[ 'lastname' ]){
                            $member->setLastName( $data[ 'lastname' ]);
                        }
                        
                        if(@$data[ 'experienceYears' ]){
                            $member->setExperienceYears($data[ 'experienceYears' ]);
                        }
                        if(@$data[ 'diplomaLevel' ]){
                            $member->setDiplomaLevel($data[ 'diplomaLevel' ]);
                        }
                        //Entity manager
                        $em = $this->getDoctrine()->getManager();
                        //Update the given contract
                        $em->merge( $member );
                        $em->flush();
                        //Return code 200
                        return $this->json([
                            'response' => 200
                        ]);
                    } catch( \Exception $e ){
                        return $this->json( [
                            "response" => "erreur lors du post",
                            "error" => $e->getMessage()
                        ]);
                    }

                } else { //If the contract doesn't exist return response
                    return $this->json([
                        'response' => 'Le type de contrat demandé n\'existe pas'
                    ]);
                }
            
            } else
            {
                return $this->json([
                    'response' => 500
                ]);
            }
        }
    }
}