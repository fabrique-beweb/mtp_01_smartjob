<?php

namespace App\Controller;

use Symfony\Component\HttpClient\HttpClient;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class ApiAccessTokenController extends AbstractController
{
    /**
     * @Route("/access_token", name="access_token")
     * @return JsonResponse
     * @throws TransportExceptionInterface
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     */
    public function index()
    {
        $client = httpClient::create();

        $response = $client->request('POST',
            'https://entreprise.pole-emploi.fr/connexion/oauth2/access_token?realm=%2Fpartenaire', [
                    'body' => [
                        'grant_type' => 'client_credentials',
                        'client_id' => 'renseignez votre identifiant',
                        'client_secret'=> 'renseignez votre clé secrete',
                        'scope' => 'application_PAR_jobsmart_6b7853cd507a44745c2bd5b8788748c0c39d20b10d1ad1daa00f524ab12eba0b api_offresdemploiv2 o2dsoffre'
                        ]
            ]);

        ;
        $data = $response->getContent();
        $data = json_decode($data, true);

        return $this->json($data);
    }
}