<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Research;
use App\Entity\Domain;
use App\Entity\PartTime;
use App\Entity\Contract;
use App\Entity\ResearchDomain;
use App\Entity\ResearchPartTime;
use App\Entity\ResearchContract;
use App\Entity\User;
use App\Entity\Pay;
use App\Services\ObjectBuilder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class ApiResearchController
 * @package App\Controller
 * @Route("/api", name="api.")
 */
class ApiResearchController extends AbstractController
{
    /**
     * @Route("/research/{id?}", name="api.research", methods={"GET", "PUT", "POST", "DELETE"})
     * @param $id
     * @param Request $request
     * @return JsonResponse
     */
    public function apiContract($id, Request $request, UserInterface $user)
    {
        if( !$id )
        {
            if( $request->isMethod( 'GET' ))
            {
                //Get all researchs entries
                $research = $this->getDoctrine()->getRepository(Research::class )->findByUser($user);
                //Init empty research list array
                $researchList = [];
                //Loop to convert all private props to public props
                for( $i = 0; $i < count( $research ); $i++ ) {
                    //Init new object
                    $researchList[ $i ] = new ObjectBuilder();
                    //Hydrate object
                    $researchList[ $i ]->setResearch( $research[ $i ] );
                }
                //Serialize object to json and return
                return new JsonResponse($researchList);
                
            } elseif ( $request->isMethod( 'POST' ))
            {
                //Get json content from the post
                $data = json_decode( $request->getContent(), true );

                if($data){
                $pay = $this->getDoctrine()->getRepository(Pay::class )->find($data[ "pay" ]);

                //Init a new research object
                $research = new Research;
                //hydrate object
                if(@$data[ "title" ]){
                    $research ->setTitle($data[ "title" ]);
                }
                if(@$data[ "location" ]){
                    $research ->setLocation($data[ "location" ]);
                }
                $research ->setPay($pay);
                $research ->setUser($user);
                    //Entity manager
                    $em = $this->getDoctrine()->getManager();
                    //Persist entity
                    $em->persist( $research );
                    //Insert to database
                    $em->flush();

                    $researchAdded= $this->getDoctrine()->getRepository(Research::class );

                    //Add all domains in relationnel table
                    $domain= $this->getDoctrine()->getRepository( Domain::class );
                    for( $i=0; $i<count( $data[ "researchDomains" ] ); $i++ ){
                        $researchDomains = new ResearchDomain();
                        //set research id
                        $researchDomains->setResearch( $researchAdded->find( $research->getId() ) );
                        //set each domain
                        $researchDomains->setDomain( $domain->find( $data[ "researchDomains" ][ $i ] ) );
                        $em = $this->getDoctrine()->getManager();
                        $em->persist( $researchDomains );
                        $em->flush();
                    }

                    //Add all contracts in relationnel table
                    $contract= $this->getDoctrine()->getRepository( Contract::class );
                    for( $i=0; $i<count( $data[ "researchContracts" ] ); $i++ ){
                        $researchContracts = new ResearchContract();
                        $researchContracts->setResearch( $researchAdded->find( $research->getId() ) );
                        $researchContracts->setContract( $contract->find( $data[ "researchContracts" ][ $i ] ) );
                        $em = $this->getDoctrine()->getManager();
                        $em->persist( $researchContracts );
                        $em->flush();
                    }

                    //Add all partTimes in relationnel table
                    $partTime= $this->getDoctrine()->getRepository( PartTime::class );
                    for( $i=0; $i<count( $data[ "researchPartTimes" ] ); $i++ ){
                        $researchPartTimes = new ResearchPartTime();
                        $researchPartTimes->setResearch( $researchAdded->find( $research->getId() ) );
                        $researchPartTimes->setPartTime( $partTime->find( $data[ "researchPartTimes" ][ $i ] ) );
                        $em = $this->getDoctrine()->getManager();
                        $em->persist( $researchPartTimes );
                        $em->flush();
                    }
                    //Send back 201 status
                    return $this->json([
                        'response' =>  201
                    ]);
                } else {
                    //If form invalid return 500 status
                    return $this->json([
                        'response' =>  500
                    ]);
                }
            } elseif ( $request->isMethod( 'PUT' ) )
            {
                //Get json from the PUT and decode it
                $data = json_decode( $request->getContent(), true );
                //Search research by the given ID
                $research = $this->getDoctrine()->getRepository( Research::class )->find( $data[ 'id' ] );
                //get pay id
                $pay = $this->getDoctrine()->getRepository(Pay::class )->find($data[ "pay" ]);

                if( $research ) // If the research at the given id exist
                {
                    try{

                        //hydrate object research
                        if(@$data[ "title" ]){
                            $research ->setTitle($data[ "title" ]);
                        }
                        if($data[ "location" ]){
                            $research ->setLocation($data[ "location" ] );
                        }
                        if($data[ "pay" ]){
                            $research ->setPay($pay);
                        }
                        $research ->setUser($user);
                        //Entity manager
                        $em = $this->getDoctrine()->getManager();
                        //Persist entity
                        $em->merge( $research );
                        //Insert to database
                        $em->flush();
                        
                        //get relationnel tables by researchID
                        $researchDomain = $this->getDoctrine()->getRepository(ResearchDomain::class )->findByResearch($data[ "id" ]);

                        $researchContract = $this->getDoctrine()->getRepository(ResearchContract::class )->findByResearch($data[ "id" ]);

                        $researchPartTime = $this->getDoctrine()->getRepository(ResearchPartTime::class )->findByResearch($data[ "id" ]);
                      

                    $researchAdded= $this->getDoctrine()->getRepository(Research::class );

               
                        if($data[ "researchDomains" ] ){ //|| count($data[ "researchDomains" ])!== count( $researchDomain ) 
                        //delete all researchDomain for id research
                            for( $i = 0; $i < count( $researchDomain ); $i++ )
                                {
                                    $em = $this->getDoctrine()->getManager();
                                    $em->remove($researchDomain[ $i ]); 
                                    $em->flush();
                                }
                            //Add all domains in relationnel table
                            $domain= $this->getDoctrine()->getRepository( Domain::class );
                            for( $i=0; $i<count( $data[ "researchDomains" ] ); $i++ ){
                                $researchDomains = new ResearchDomain();
                                //set research id
                                $researchDomains->setResearch( $researchAdded->find( $research->getId() ) );
                                //set each domain
                                $researchDomains->setDomain( $domain->find( $data[ "researchDomains" ][ $i ] ) );
                                $em = $this->getDoctrine()->getManager();
                                $em->persist( $researchDomains );
                                $em->flush();
                            }
                        }

                        if($data[ "researchContracts" ]){
                            //delete all researchContract for id research
                            for( $j = 0; $j < count( $researchContract ); $j++ )
                            {
                                $em = $this->getDoctrine()->getManager();
                                $em->remove( $researchContract[ $j ] );
                                $em->flush();
                            }
                            //Add all contracts in relationnel table
                            $contract= $this->getDoctrine()->getRepository( Contract::class );
                            for( $i=0; $i<count( $data[ "researchContracts" ] ); $i++ ){
                                $researchContracts = new ResearchContract();
                                $researchContracts->setResearch( $researchAdded->find( $research->getId() ) );
                                $researchContracts->setContract( $contract->find( $data[ "researchContracts" ][ $i ] ) );
                                $em = $this->getDoctrine()->getManager();
                                $em->persist( $researchContracts );
                                $em->flush();
                            }
                        }

                        if($data[ "researchPartTimes" ]){ 
                        //delete all researchPartTime for id research
                        for( $k = 0; $k < count( $researchPartTime ); $k++ )
                        {
                            $em = $this->getDoctrine()->getManager();
                            $em->remove( $researchPartTime[ $k ] );
                            $em->flush();
                        }
                        //Add all partTimes in relationnel table
                        $partTime= $this->getDoctrine()->getRepository( PartTime::class );
                        for( $i=0; $i<count( $data[ "researchPartTimes" ] ); $i++ ){
                            $researchPartTimes = new ResearchPartTime();
                            $researchPartTimes->setResearch( $researchAdded->find( $research->getId() ) );
                            $researchPartTimes->setPartTime( $partTime->find( $data[ "researchPartTimes" ][ $i ] ) );
                            $em = $this->getDoctrine()->getManager();
                            $em->persist( $researchPartTimes );
                            $em->flush();
                        }
                    }
                            //Return code 200
                            return $this->json([
                                'response' => 200
                            ]);
                    } catch( \Exception $e ){
                        return $this->json([
                            "response" => "erreur lors de la requete",
                            "error" => $e->getMessage()
                        ]);
                    }

                } else { //If the reseach doesn't exist return response
                    return $this->json([
                        'response' => 'La recherche demandée n\'existe pas'
                    ]);
                }
            }
        } elseif ( $id )
        {
            if ( $request->isMethod( 'GET' ) )
            {   
                //$id = base64_decode($id);
                $research = $this->getDoctrine()->getRepository(Research::class )->find( $id );

                $temp = new ObjectBuilder();

                $temp->setResearch( $research );

                $json = new JsonResponse( $temp );

                return $json;

            } elseif ( $request->isMethod( 'DELETE' ) )
            {
                //Search research by the given ID

                $research = $this->getDoctrine()->getRepository( Research::class )->find( $id );

                if ( !$research ) {
                        //If the research doesn't exist return response
                    return $this->json([
                        'response' => 'La research demandée n\'existe pas'
                    ]);
                } else {
                    try {

                        //get relationnel tables by researchID
                        $researchDomain = $this->getDoctrine()->getRepository(ResearchDomain::class )->findByResearch($id);

                        $researchContract = $this->getDoctrine()->getRepository(ResearchContract::class )->findByResearch($id);

                        $researchPartTime = $this->getDoctrine()->getRepository(ResearchPartTime::class )->findByResearch($id);
                    

                //delete all relationnel tables
                        //delete all researchDomain for id research
                        for( $i = 0; $i < count( $researchDomain ); $i++ )
                        {
                            $em = $this->getDoctrine()->getManager();
                            $em->remove($researchDomain[ $i ]); 
                            $em->flush();
                        }
                        //delete all researchContract for id research
                        for( $j = 0; $j < count( $researchContract ); $j++ )
                        {
                            $em = $this->getDoctrine()->getManager();
                            $em->remove( $researchContract[ $j ] );
                            $em->flush();
                        }
                        //delete all researchPartTime for id research
                        for( $k = 0; $k < count( $researchPartTime ); $k++ )
                        {
                            $em = $this->getDoctrine()->getManager();
                            $em->remove( $researchPartTime[ $k ] );
                            $em->flush();
                        }

                        //delete research
                        $em = $this->getDoctrine()->getManager();
                        $em->remove( $research );
                        $em->flush();

                        return $this->json([
                            'response' => '200'
                        ]);
                    }catch( \Exception $e ){
                        return $this->json([
                            "response" => "erreur lors de la requete",
                            "error" => $e->getMessage()
                        ]);
                    }
                }
            }
        } else
        {
        return $this->json([
            'response' => '500'
        ]);
        }
    }
}

