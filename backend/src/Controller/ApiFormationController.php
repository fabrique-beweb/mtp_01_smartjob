<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Formation;
use App\Entity\User;
use App\Services\ObjectBuilder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * Class ApiFormationController
 * @package App\Controller
 * @Route("/api", name="api.")
 */
class ApiFormationController extends AbstractController
{
    /**
     * @Route("/formation/{id?}", name="api.formation", methods={"GET", "PUT", "POST", "DELETE"})
     * @param $id
     * @param Request $request
     * @return JsonResponse
     */
    public function apiFormation($id, Request $request, UserInterface $user)
    {
        if( !$id )
        {
            if( $request->isMethod( 'GET' ))
            {
                //Get all formations entries
                $formation = $this->getDoctrine()->getRepository(Formation::class )->findByUser($user);
                //Init empty formations list array
                $formationList = [];
                //Loop to convert all private props to public props
                for( $i = 0; $i < count( $formation ); $i++ ) {
                    //Init new object
                    $formationList[ $i ] = new ObjectBuilder();
                    //Hydrate object
                    $formationList[ $i ]->setFormation( $formation[ $i ] );
                }
                //Serialize object to json and return
                return new JsonResponse($formationList);
                
            } elseif ( $request->isMethod( 'POST' ))
            {
                //Get json content from the post
                $data = json_decode( $request->getContent(), true );

                //$formationAdded= $this->getDoctrine()->getRepository(Formation::class );

                    //Init a new formation object
                    $formation = new Formation;

                    //hydrate object
                    $formation ->setTitle($data[ "title" ]);
                    $formation ->setLevel($data[ "level" ]);
                    $formation ->setSchool($data[ "school" ]);
                    $formation ->setLocation($data[ "location" ]);
                    $formation ->setDescription($data[ "description" ]);
                    $formation ->setStartDate(new \DateTime((string)$data[ "startDate" ]));
                    $formation ->setEndDate(new \DateTime((string)$data[ "endDate" ]));
                    $formation ->setUser($user);

                    //Persist entity
                    $em = $this->getDoctrine()->getManager();
                    $em->persist( $formation );
                    //Insert to database
                    $em->flush();
                
                //Send back 201 status
                return $this->json([
                    'response' =>  201
                ]);
            } elseif ( $request->isMethod( 'PUT' ) )
            {
                //Get json from the PUT and decode it
                $data = json_decode( $request->getContent(), true );
                //Search formation by the given ID
                $formation = $this->getDoctrine()->getRepository( Formation::class )->find( $data[ "id" ] );

                if( $formation ) // If the formation at the given id exist
                {
                    try{
                        //delete formation by id
                        $em = $this->getDoctrine()->getManager();
                        $em->remove($formation);
                        $em->flush();

                            //Init a new formation object
                            $formation = new Formation;
        
                            //hydrate object
                            $formation ->setTitle($data[ "title" ]);
                            $formation ->setLevel($data[ "level" ]);
                            $formation ->setSchool($data[ "school" ]);
                            $formation ->setLocation($data[ "location" ]);
                            $formation ->setDescription($data[ "description" ]);
                            $formation ->setStartDate(new \DateTime((string)$data[ "startDate" ]));
                            $formation ->setEndDate(new \DateTime((string)$data[ "endDate" ]));
                            $formation ->setUser($user);

                            //Persist entity
                            $em = $this->getDoctrine()->getManager();
                            $em->persist( $formation );
                            //Insert to database
                            $em->flush();
                        //Return code 200
                        return $this->json([
                            'response' => 200
                        ]);
                    } catch( \Exception $e ){
                        return $this->json( [
                            "error" => $e->getMessage()
                        ] );
                    }

                } else { //If the formation doesn't exist return response
                    return $this->json([
                        'response' => 'La formation demandée n\'existe pas'
                    ]);
                }
            }
        } elseif ( $id )
        {
            if ( $request->isMethod( 'GET' ) )
            {   
                //$id = base64_decode($id);
                $formation = $this->getDoctrine()->getRepository(Formation::class )->find( $id );

                $temp = new ObjectBuilder();

                $temp->setFormation( $formation );

                $json = new JsonResponse( $temp );

                return $json;

            } elseif ( $request->isMethod( 'DELETE' ) )
            {
                //Search formation by the given ID
                $formation = $this->getDoctrine()->getRepository( Formation::class )->find( $id );

                if ( !$formation ) {
                         //If the formation doesn't exist return response
                    return $this->json([
                        'response' => 'Le formation demandée n\'existe pas'
                    ]);
                } else {

                    $formation= $this->getDoctrine()->getRepository(Formation::class)->find($id); 

                    $em = $this->getDoctrine()->getManager();
                    $em->remove($formation);
                    $em->flush();

                    return $this->json([
                        'response' => '200'
                    ]);
                }
            }
        } else
            {
            return $this->json([
                'response' => 500
            ]);
        }
    }
}