<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Experience;
use App\Entity\User;
use App\Services\ObjectBuilder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class ApiExperienceController
 * @package App\Controller
 * @Route("/api", name="api.")
 */
class ApiExperienceController extends AbstractController
{
    /**
     * @Route("/experience/{id?}", name="api.experience", methods={"GET", "PUT", "POST", "DELETE"})
     * @param $id
     * @param Request $request
     * @return JsonResponse
     */
    public function apiExperience($id, Request $request, UserInterface $user)
    {
        if( !$id )
        {
            if( $request->isMethod( 'GET' ))
            {
                //Get all experiences entries
                $experience = $this->getDoctrine()->getRepository(Experience::class )->findByUser($user);
                //Init empty experiences list array
                $experienceList = [];
                //Loop to convert all private props to public props
                for( $i = 0; $i < count( $experience ); $i++ ) {
                    //Init new object
                    $experienceList[ $i ] = new ObjectBuilder();
                    //Hydrate object
                    $experienceList[ $i ]->setExperience( $experience[ $i ] );
                }
                //Serialize object to json and return
                return new JsonResponse($experienceList);
                
            } elseif ( $request->isMethod( 'POST' ))
            {
                //Get json content from the post
                $data = json_decode( $request->getContent(), true );

                //$experienceAdded= $this->getDoctrine()->getRepository(experience::class );

                    //Init a new experience object
                    $experience = new Experience;

                    //hydrate object
                    $experience ->setTitle($data[ "title" ]);
                    $experience ->setCompany($data[ "company" ]);
                    $experience ->setLocation($data[ "location" ]);
                    $experience ->setDescription($data[ "description" ]);
                    $experience ->setStartDate(new \DateTime((string)$data[ "startDate" ]));
                    $experience ->setEndDate(new \DateTime((string)$data[ "endDate" ]));
                    $experience ->setUser($user);

                    //Persist entity
                    $em = $this->getDoctrine()->getManager();
                    $em->persist( $experience );
                    //Insert to database
                    $em->flush();
                
                //Send back 201 status
                return $this->json([
                    'response' =>  201
                ]);
            } elseif ( $request->isMethod( 'PUT' ) )
            {
                //Get json from the PUT and decode it
                $data = json_decode( $request->getContent(), true );
                //Search experience by the given ID
                $experience = $this->getDoctrine()->getRepository( Experience::class )->find( $data[ "id" ] );

                if( $experience ) // If the experience at the given id exist
                {
                    try{
                        //delete experience by id
                        $em = $this->getDoctrine()->getManager();
                        $em->remove($experience);
                        $em->flush();

                            //Init a new experience object
                            $experience = new Experience;
        
                            //hydrate object
                            $experience ->setTitle($data[ "title" ]);
                            $experience ->setCompany($data[ "company" ]);
                            $experience ->setLocation($data[ "location" ]);
                            $experience ->setDescription($data[ "description" ]);
                            $experience ->setStartDate(new \DateTime((string)$data[ "startDate" ]));
                            $experience ->setEndDate(new \DateTime((string)$data[ "endDate" ]));
                            $experience ->setUser($user);

                            //Persist entity
                            $em = $this->getDoctrine()->getManager();
                            $em->persist( $experience );
                            //Insert to database
                            $em->flush();
                        
                        //Return code 200
                        return $this->json([
                            'response' => 200
                        ]);
                    } catch( \Exception $e ){
                        return $this->json( [
                            "error" => $e->getMessage()
                        ] );
                    }

                } else { //If the experience doesn't exist return response
                    return $this->json([
                        'response' => 'L\'experience demandée n\'existe pas'
                    ]);
                }
            }
        } elseif ( $id )
        {
            if ( $request->isMethod( 'GET' ) )
            {   
                //$id = base64_decode($id);
                $experience = $this->getDoctrine()->getRepository(Experience::class )->find( $id );

                $temp = new ObjectBuilder();

                $temp->setExperience( $experience );

                $json = new JsonResponse( $temp );

                return $json;

            } elseif ( $request->isMethod( 'DELETE' ) )
            {
                //Search experience by the given ID
                $experience = $this->getDoctrine()->getRepository( Experience::class )->find( $id );

                if ( !$experience ) {
                         //If the experience doesn't exist return response
                    return $this->json([
                        'response' => 'L\'experience demandée n\'existe pas'
                    ]);
                } else {

                    $experience= $this->getDoctrine()->getRepository(Experience::class)->find($id); 

                    $em = $this->getDoctrine()->getManager();
                    $em->remove($experience);
                    $em->flush();

                    return $this->json([
                        'response' => '200'
                    ]);
                }
            }
        } else
            {
            return $this->json([
                'response' => 500
            ]);
        }
    }
}