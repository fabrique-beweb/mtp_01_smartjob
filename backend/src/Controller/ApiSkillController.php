<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Skill;
use App\Entity\User;
use App\Services\ObjectBuilder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class ApiSkillController
 * @package App\Controller
 * @Route("/api", name="api.")
 */
class ApiSkillController extends AbstractController
{
    /**
     * @Route("/skill/{id?}", name="api.skill", methods={"GET", "PUT", "POST", "DELETE"})
     * @param $id
     * @param Request $request
     * @return JsonResponse
     */
    public function apiSkill($id, Request $request, UserInterface $user)
    {
        if( !$id )
        {
            if( $request->isMethod( 'GET' ))
            {
                //Get all skills entries
                $skill = $this->getDoctrine()->getRepository(Skill::class )->findByUser($user);
                //Init empty skills list array
                $skillList = [];
                //Loop to convert all private props to public props
                for( $i = 0; $i < count( $skill ); $i++ ) {
                    //Init new object
                    $skillList[ $i ] = new ObjectBuilder();
                    //Hydrate object
                    $skillList[ $i ]->setSkill( $skill[ $i ] );
                }
                //Serialize object to json and return
                return new JsonResponse($skillList);
                
            } elseif ( $request->isMethod( 'POST' ))
            {
                //Get json content from the post
                $data = json_decode( $request->getContent(), true );

                $skillAdded= $this->getDoctrine()->getRepository(Skill::class );

                for( $i=0; $i<count( $data); $i++ ){
                    //Init a new skill object
                    $skill = new Skill;

                    $skill->setName($data[$i][ "name" ]);
                    $skill->setUser($user);
                    $em = $this->getDoctrine()->getManager();

                    //Persist entity
                    $em->persist( $skill );
                    //Insert to database
                    $em->flush();
                }
                //Send back 201 status
                return $this->json([
                    'response' =>  201
                ]);
            } else {
                //If form invalid return 500 status
                return $this->json([
                    'response' =>  500
                ]);
            }

        } elseif ( $id )
        {
            if ( $request->isMethod( 'GET' ) )
            {   
                //$id = base64_decode($id);
                $skill = $this->getDoctrine()->getRepository(Skill::class )->find( $id );

                $temp = new ObjectBuilder();

                $temp->setSkill( $skill );

                $json = new JsonResponse( $temp );

                return $json;

            } elseif ( $request->isMethod( 'DELETE' ) )
            {
                //Search skill by the given ID
                $skill = $this->getDoctrine()->getRepository( Skill::class )->find( $id );

                if ( !$skill ) {
                         //If the skill doesn't exist return response
                    return $this->json([
                        'response' => 'Le skill demandé n\'existe pas'
                    ]);
                } else {

                    $skill= $this->getDoctrine()->getRepository(Skill::class)->find($id); 

                    $em = $this->getDoctrine()->getManager();
                    $em->remove($skill);
                    $em->flush();

                    return $this->json([
                        'response' => '200'
                    ]);
                }
            }
        } else
            {
            return $this->json([
                'response' => 500
            ]);
        }
    }
}

