<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;
use App\Services\ObjectBuilder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserInterface;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin", name="admin")
     * @param $id
     * @param Request $request
     * @return JsonResponse
     */
    public function Admin()
    {
        if( !$id ) 
        {
            if( $request->isMethod( 'GET' )) //pour l'admin
            {
                //Get all members entries
                $members = $this->getDoctrine()->getRepository(User::class )->findAll();
                //Init empty members list array
                $memberList = [];
                //Loop to convert all private props to public props
                for( $i = 0; $i < count( $member ); $i++ ) {
                    //Init new object
                    $memberList[ $i ] = new ObjectBuilder();
                    //Hydrate object
                    $memberList[ $i ]->setMember( $member[ $i ] );
                }
                //Serialize object to json and return
                return new JsonResponse($memberList);
            } 
        }
    }
}
