<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use App\Service\FileUploader;

/**
 * Class ApiRegistrationController
 * @package App\Controller
 */
class ApiRegistrationController extends AbstractController
{
    /**
     * @Route("/register", name="register", methods={"POST"})
     * @param Request $request
     * @param UserPasswordEncoderInterface $encoder
     * @return JsonResponse
     */
    public function register( Request $request, UserPasswordEncoderInterface $encoder )
    {
        $data = json_decode( $request->getContent(), true );
        if( $request->isMethod( 'POST' ) ) {
            
            //verifie que l'email n'est pas deja present dans la table user
            $AllUsers = $this->getDoctrine()->getRepository(User::class )->findAll();
            for ($j=0; $j<count($AllUsers); $j++){
                if($data["email"] == $AllUsers[$j]->getEmail()){
                    return $this->json([
                        'response' =>  'l\'email '.$AllUsers[$j]->getEmail().' existe déjà'
                        ]);
                }
            }

            $user = new User();
        
            $user->getUsername( $data[ 'email' ] );

            $user->setEmail( $data[ 'email' ] );
            $plainPassword = $data[ 'password' ];
            $user->setPassword( $encoder->encodePassword( $user, $plainPassword ) );
            $user->setFirstName( $data[ 'firstname' ]);
            $user->setLastName( $data[ 'lastname' ]);
            $user->setAvatar( "default.jpeg");

            //$user->setRoles( 'ROLE_USER' );
            //$user->setRoles( 'ROLE_ADMIN' );
            //$user->setRoles( 'ROLE_ANONYM' );

            $em = $this->getDoctrine()->getManager();

            $em->persist( $user );

            $em->flush();
            return $this->json([
                'response' => 201
            ]);
        }
        return $this->json([
            'response' => 500
        ]);
    }
}
