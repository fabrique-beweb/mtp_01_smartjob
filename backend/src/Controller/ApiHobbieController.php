<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Hobbie;
use App\Entity\User;
use App\Services\ObjectBuilder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserInterface;


/**
 * Class ApiHobbieController
 * @package App\Controller
 * @Route("/api", name="api.")
 */
class ApiHobbieController extends AbstractController
{
    /**
     * @Route("/hobbie/{id?}", name="api.hobbie", methods={"GET", "PUT", "POST", "DELETE"})
     * @param $id
     * @param Request $request
     * @return JsonResponse
     */
    public function apiHobbie($id, Request $request, UserInterface $user)
    {
        if( !$id )
        {
            if( $request->isMethod( 'GET' ))
            {
                //Get all hobbies entries
                $hobbie = $this->getDoctrine()->getRepository(Hobbie::class )->findByUser($user);
                //Init empty hobbies list array
                $hobbieList = [];
                //Loop to convert all private props to public props
                for( $i = 0; $i < count( $hobbie ); $i++ ) {
                    //Init new object
                    $hobbieList[ $i ] = new ObjectBuilder();
                    //Hydrate object
                    $hobbieList[ $i ]->setHobbie( $hobbie[ $i ] );
                }
                //Serialize object to json and return
                return new JsonResponse($hobbieList);
                
            } elseif ( $request->isMethod( 'POST' ))
            {
                //Get json content from the post
                $data = json_decode( $request->getContent(), true );

                $hobbieAdded= $this->getDoctrine()->getRepository(Hobbie::class );

                for( $i=0; $i<count( $data); $i++ ){
                    //Init a new hobbie object
                    $hobbie = new Hobbie;

                    $hobbie ->setName($data[ $i ][ "name" ]);
                    $hobbie ->setUser($user);
                    $em = $this->getDoctrine()->getManager();

                    //Persist entity
                    $em->persist( $hobbie );
                    //Insert to database
                    $em->flush();
                }
                //Send back 201 status
                return $this->json([
                    'response' =>  201
                ]);
            } else {
                //If form invalid return 500 status
                return $this->json([
                    'response' =>  500
                ]);
            }

        } elseif ( $id )
        {
            if ( $request->isMethod( 'GET' ) )
            {   
                //$id = base64_decode($id);
                $hobbie = $this->getDoctrine()->getRepository(Hobbie::class )->find( $id );

                $temp = new ObjectBuilder();

                $temp->setHobbie( $hobbie );

                $json = new JsonResponse( $temp );

                return $json;

            } elseif ( $request->isMethod( 'DELETE' ) )
            {
                //Search hobbie by the given ID
                $hobbie = $this->getDoctrine()->getRepository( Hobbie::class )->find( $id );

                if ( !$hobbie ) {
                         //If the hobbie doesn't exist return response
                    return $this->json([
                        'response' => 'Le hobbie demandé n\'existe pas'
                    ]);
                } else {

                    $hobbie= $this->getDoctrine()->getRepository(Hobbie::class)->find($id); 

                    $em = $this->getDoctrine()->getManager();
                    $em->remove($hobbie);
                    $em->flush();

                    return $this->json([
                        'response' => '200'
                    ]);
                }
            }
        } else
            {
            return $this->json([
                'response' => 500
            ]);
        }
    }

}
