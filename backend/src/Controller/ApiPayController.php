<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Pay;
use App\Entity\Research;
use App\Entity\User;
use App\Services\ObjectBuilder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class ApiPayController
 * @package App\Controller
 * @Route("/api", name="api.")
 */
class ApiPayController extends AbstractController
{
    /**
     * @Route("/pay/{id?}", name="api.pay", methods={"GET", "PUT", "POST", "DELETE"})
     * @param $id
     * @param Request $request
     * @return JsonResponse
     */
    public function apiPay($id, Request $request)
    {
        if( !$id )
        {
            if( $request->isMethod( 'GET' ))
            {
                //Get all pays entries
                $pay = $this->getDoctrine()->getRepository(Pay::class )->findAll();
                //Init empty pay list array
                $payList = [];
                //Loop to convert all private props to public props
                for( $i = 0; $i < count( $pay ); $i++ ) {
                    //Init new object
                    $payList[ $i ] = new ObjectBuilder();
                    //Hydrate object
                    $payList[ $i ]->setpay( $pay[ $i ] );
                }
                //Serialize object to json
                return new JsonResponse($payList);

            } elseif ( $request->isMethod( 'POST' ))
            {
                //Get json content from the post
                $data = json_decode( $request->getContent(), true );

                //verifie que le payType n'est pas déja present dans la table 
                $Allpay = $this->getDoctrine()->getRepository(Pay::class )->findAll();
                for ( $j=0; $j < count( $Allpay ); $j++ )
                {
                    if( $data[ "payRange" ] === $Allpay[ $j ]->getPayRange() )
                    {
                        return $this->json( [
                            'response' =>  'la tranche salariale '.$Allpay[$j]->getPayRange().' existe déjà dans la table pay'
                            ] );
                    }
                }

                //Init a new pay object
                $pay = new Pay;

                //hydrate object
                $pay->setPayRange( $data[ 'payRange' ] );

                //Entity manager
                $em = $this->getDoctrine()->getManager();
                //Persist entity
                $em->persist($pay);
                //Insert to database
                $em->flush();
                //Send back 201 status
                return $this->json([
                    'response' =>  '201'
                ]);
            } elseif ( $request->isMethod( 'PUT' ) )
            {
                //Get json from the PUT and decode it
                $data = json_decode( $request->getContent(), true );
                //Search pay by the given ID
                $pay = $this->getDoctrine()->getRepository( Pay::class )->find( $data[ 'id' ] );

                if( $pay ) // If the pay at the given id exist
                {
                    try{
                        $pay->setPayRange( $data[ 'payRange' ] );
                        //Entity manager
                        $em = $this->getDoctrine()->getManager();
                        //Update the given pay
                        $em->merge( $pay );
                        $em->flush();
                        //Return code 200
                        return $this->json([
                            'response' => 200
                        ]);
                    } catch( \Exception $e ){
                        return $this->json( [
                            "response" => "la tranche salariale : ". $data['payRange']." existe deja",
                            "error" => $e->getMessage()
                        ] );
                    }

                } else { //If the pay doesn't exist return response
                    return $this->json([
                        'response' => 'La tranche salariale demandée n\'existe pas'
                    ]);
                }
            }

        } elseif ( $id )
        {
            if ( $request->isMethod( 'GET' ) )
            {
                $pay = $this->getDoctrine()->getRepository(Pay::class )->find( $id );

                $temp = new ObjectBuilder();

                $temp->setPay( $pay );

                return new JsonResponse( $temp );

            } elseif ( $request->isMethod( 'DELETE' ) )
            {

                $research = $this->getDoctrine()->getRepository( Research::class )->findBy( [ 'pay' => $id ] );
                $pay = $this->getDoctrine()->getRepository( Pay::class )->find( $id );

                if ( $research ) {

                    return new JsonResponse( ['response' => 'Attention, cette tranche salariale a déja été choisie dans les recherches d\'utilisateurs, vous ne pouvez pas la supprimer']);
                } else
                {
                    if( $pay ) // If the pay at the given id exist
                    {
                        //Entity manager
                        $em = $this->getDoctrine()->getManager();
                        //Remove the given pay
                        $em->remove( $pay );
                        $em->flush();
                        //Return code 200
                        return $this->json( [
                            'response' => 200
                        ] );
                    } else { //If the pay doesn't exist return response
                        return $this->json( [
                            'response' => 'La tranche salariale demandée n\'existe pas'
                        ] );
                    }
                }
            }
        } else
            {
            return $this->json( [
                'response' => 500
            ] );
        }
    }
}

