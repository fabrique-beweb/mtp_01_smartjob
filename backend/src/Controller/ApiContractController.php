<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Contract;
use App\Entity\ResearchContract;
use App\Entity\User;
use App\Services\ObjectBuilder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class ApiContractController
 * @package App\Controller
 * @Route("/api", name="api.")
 */
class ApiContractController extends AbstractController
{
    /**
     * @Route("/contract/{id?}", name="api.contract", methods={"GET", "PUT", "POST", "DELETE"})
     * @param $id
     * @param Request $request
     * @return JsonResponse
     */
    public function apiContract($id, Request $request)
    {
        if( !$id )
        {
            if( $request->isMethod( 'GET' ))
            {
                //Get all contracts entries
                $contract = $this->getDoctrine()->getRepository(Contract::class )->findAll();
                //Init empty contract list array
                $contractList = [];
                //Loop to convert all private props to public props
                for( $i = 0; $i < count( $contract ); $i++ ) {
                    //Init new object
                    $contractList[ $i ] = new ObjectBuilder();
                    //Hydrate object
                    $contractList[ $i ]->setContract( $contract[ $i ] );
                }
                //Serialize object to json
                return new JsonResponse($contractList);

            } elseif ( $request->isMethod( 'POST' ))
            {
                //Get json content from the post
                $data = json_decode( $request->getContent(), true );

                //verifie que le contractType n'est pas déja present dans la table 
                $Allcontract = $this->getDoctrine()->getRepository(Contract::class )->findAll();
                for ( $j=0; $j < count( $Allcontract ); $j++ )
                {
                    if( $data[ "name" ] === $Allcontract[ $j ]->getName() )
                    {
                        return $this->json( [
                            'response' =>  'le type de contrat : '.$Allcontract[$j]->getName().' existe déjà dans la table contract'
                            ] );
                    }
                }

                //Init a new contract object
                $contract = new Contract;

                //hydrate object
                $contract->setName( $data[ 'name' ] );

                //Entity manager
                $em = $this->getDoctrine()->getManager();
                //Persist entity
                $em->persist($contract);
                //Insert to database
                $em->flush();
                //Send back 201 status
                return $this->json([
                    'response' =>  '201'
                ]);
            } elseif ( $request->isMethod( 'PUT' ) )
            {
                //Get json from the PUT and decode it
                $data = json_decode( $request->getContent(), true );
                //Search contract by the given ID
                $contract = $this->getDoctrine()->getRepository( Contract::class )->find( $data[ 'id' ] );

                if( $contract ) // If the contract at the given id exist
                {
                    try{
                        $contract->setName( $data[ 'name' ] );
                        //Entity manager
                        $em = $this->getDoctrine()->getManager();
                        //Update the given contract
                        $em->merge( $contract );
                        $em->flush();
                        //Return code 200
                        return $this->json([
                            'response' => 200
                        ]);
                    } catch( \Exception $e ){
                        return $this->json( [
                            "response" => "le type de contrat : ". $data['name']." existe déjà",
                            "error" => $e->getMessage()
                        ] );
                    }

                } else { //If the contract doesn't exist return response
                    return $this->json([
                        'response' => 'Le type de contrat demandé n\'existe pas'
                    ]);
                }
            }

        } elseif ( $id )
        {
            if ( $request->isMethod( 'GET' ) )
            {
                $contract = $this->getDoctrine()->getRepository(Contract::class )->find( $id );

                $temp = new ObjectBuilder();

                $temp->setContract( $contract );

                return new JsonResponse( $temp );

            } elseif ( $request->isMethod( 'DELETE' ) )
            {

                $research_contract = $this->getDoctrine()->getRepository( ResearchContract::class )->findBy( [ 'contract' => $id ] );
                $contract = $this->getDoctrine()->getRepository( Contract::class )->find( $id );

                if ( $research_contract ) {
                    return new JsonResponse( ['response' => 'Attention, ce type de contrat a déja été choisi dans les recherches d\'utilisateurs, vous ne pouvez pas le supprimer']);
                } else
                {
                    if( $contract ) // If the contract at the given id exist
                    {
                        //Entity manager
                        $em = $this->getDoctrine()->getManager();
                        //Remove the given contract
                        $em->remove( $contract );
                        $em->flush();
                        //Return code 200
                        return $this->json( [
                            'response' => 200
                        ] );
                    } else { //If the contract doesn't exist return response
                        return $this->json( [
                            'response' => 'Le type de contrat demandé n\'existe pas'
                        ] );
                    }
                }
            }
        } else
            {
            return $this->json( [
                'response' => 500
            ] );
        }
    }
}