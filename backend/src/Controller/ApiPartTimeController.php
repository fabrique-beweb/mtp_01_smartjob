<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\PartTime;
use App\Entity\ResearchPartTime;
use App\Entity\User;
use App\Services\ObjectBuilder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class ApiPartTimeController
 * @package App\Controller
 * @Route("/api", name="api.")
 */
class ApiPartTimeController extends AbstractController
{
    /**
     * @Route("/partTime/{id?}", name="api.partTime", methods={"GET", "PUT", "POST", "DELETE"})
     * @param $id
     * @param Request $request
     * @return JsonResponse
     */
    public function apiPartTime($id, Request $request)
    {
        if( !$id )
        {
            if( $request->isMethod( 'GET' ))
            {
                //Get all partTimes entries
                $partTime = $this->getDoctrine()->getRepository(PartTime::class )->findAll();
                //Init empty partTime list array
                $partTimeList = [];
                //Loop to convert all private props to public props
                for( $i = 0; $i < count( $partTime ); $i++ ) {
                    //Init new object
                    $partTimeList[ $i ] = new ObjectBuilder();
                    //Hydrate object
                    $partTimeList[ $i ]->setPartTime( $partTime[ $i ] );
                }
                //Serialize object to json
                return new JsonResponse($partTimeList);

            } elseif ( $request->isMethod( 'POST' ))
            {
                //Get json content from the post
                $data = json_decode( $request->getContent(), true );

                //verifie que le partTimeType n'est pas déja present dans la table 
                $AllpartTime = $this->getDoctrine()->getRepository(PartTime::class )->findAll();
                for ( $j=0; $j < count( $AllpartTime ); $j++ )
                {
                    if( $data[ "name" ] === $AllpartTime[ $j ]->getName() )
                    {
                        return $this->json( [
                            'response' =>  'le type : '.$AllpartTime[$j]->getName().' existe déjà dans la table partTime'
                            ] );
                    }
                }

                //Init a new partTime object
                $partTime = new PartTime;

                //hydrate object
                $partTime->setName( $data[ 'name' ] );

                //Entity manager
                $em = $this->getDoctrine()->getManager();
                //Persist entity
                $em->persist($partTime);
                //Insert to database
                $em->flush();
                //Send back 201 status
                return $this->json([
                    'response' =>  '201'
                ]);
            } elseif ( $request->isMethod( 'PUT' ) )
            {
                //Get json from the PUT and decode it
                $data = json_decode( $request->getContent(), true );
                //Search partTime by the given ID
                $partTime = $this->getDoctrine()->getRepository( PartTime::class )->find( $data[ 'id' ] );

                if( $partTime ) // If the partTime at the given id exist
                {
                    try{
                        $partTime->setName( $data[ 'name' ] );
                        //Entity manager
                        $em = $this->getDoctrine()->getManager();
                        //Update the given partTime
                        $em->merge( $partTime );
                        $em->flush();
                        //Return code 200
                        return $this->json([
                            'response' => 200
                        ]);
                    } catch( \Exception $e ){
                        return $this->json( [
                            "response" => "le type : ". $data['name']." existe déjà",
                            "error" => $e->getMessage()
                        ] );
                    }

                } else { //If the partTime doesn't exist return response
                    return $this->json([
                        'response' => 'Le type demandé n\'existe pas'
                    ]);
                }
            }

        } elseif ( $id )
        {
            if ( $request->isMethod( 'GET' ) )
            {
                $partTime = $this->getDoctrine()->getRepository(PartTime::class )->find( $id );

                $temp = new ObjectBuilder();

                $temp->setPartTime( $partTime );

                return new JsonResponse( $temp );

            } elseif ( $request->isMethod( 'DELETE' ) )
            {

                $research_partTime = $this->getDoctrine()->getRepository( ResearchPartTime::class )->findBy( [ 'partTime' => $id ] );
                $partTime = $this->getDoctrine()->getRepository( PartTime::class )->find( $id );

                if ( $research_partTime ) {

                    return new JsonResponse( ['response' => 'Attention, ce type a déja été choisi dans les recherches d\'utilisateurs, vous ne pouvez pas le supprimer']);
                } else
                {
                    if( $partTime ) // If the partTime at the given id exist
                    {
                        //Entity manager
                        $em = $this->getDoctrine()->getManager();
                        //Remove the given partTime
                        $em->remove( $partTime );
                        $em->flush();
                        //Return code 200
                        return $this->json( [
                            'response' => 200
                        ] );
                    } else { //If the partTime doesn't exist return response
                        return $this->json( [
                            'response' => 'Le type demandé n\'existe pas'
                        ] );
                    }
                }
            }
        } else
            {
            return $this->json( [
                'response' => 500
            ] );
        }
    }
}